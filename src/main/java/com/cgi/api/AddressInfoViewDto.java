package com.cgi.api;

import java.util.*;

public class AddressInfoViewDto {

	private List<AddressBasicViewDto> addresses = new ArrayList<>();
	private List<AddressPersonGroupByCity> personsInCities = new ArrayList<>();

	public List<AddressBasicViewDto> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<AddressBasicViewDto> addresses) {
		this.addresses = addresses;
	}

	public List<AddressPersonGroupByCity> getPersonsInCities() {
		return personsInCities;
	}

	public void setPersonsInCities(List<AddressPersonGroupByCity> personsInCities) {
		this.personsInCities = personsInCities;
	}

}
