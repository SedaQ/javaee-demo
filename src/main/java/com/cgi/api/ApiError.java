package com.cgi.api;

public class ApiError {

	private long timestamp;
	private String message;
	private int statusCode;
	private String error;
	private String path;

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		return "ApiError [timestamp=" + timestamp + ", message=" + message + ", statusCode=" + statusCode + ", error="
				+ error + ", path=" + path + "]";
	}

}
