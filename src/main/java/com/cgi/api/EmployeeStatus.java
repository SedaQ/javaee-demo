package com.cgi.api;

public enum EmployeeStatus {

	ACTIVE("active..") {
		@Override
		public void print() {

		}
	},
	PASSIVE("passive..") {
		@Override
		public void print() {

		}
	};

	private String name;

	EmployeeStatus(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	abstract void print();

}
