package com.cgi.api;

public class UserCreateRequestDto {

	private String email;
	private String givenName;

	// ...
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

}
