package com.cgi.data.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import com.cgi.api.AddressBasicViewDto;
import com.cgi.api.AddressPersonGroupByCity;
import com.cgi.data.exceptions.DataAccessException;

@Stateless
public class AddressDao {

	@Resource(lookup = "jdbc/bootcampCGI")
	private DataSource dataSource;

	public List<AddressBasicViewDto> findAll() {
		List<AddressBasicViewDto> addresses = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement
						.executeQuery("SELECT id_address, city, house_number, street, zip_code FROM address a")) {
			while (rs.next()) {
				AddressBasicViewDto address = new AddressBasicViewDto();
				address.setId(rs.getLong("id_address"));
				address.setCity(rs.getString("city"));
				address.setHouseNumber(rs.getLong("house_number"));
				address.setStreet(rs.getString("street"));
				address.setZipCode(rs.getString("zip_code"));
				addresses.add(address);
			}
			return addresses;
		} catch (SQLException e) {
			throw new DataAccessException("Addresses not found..", e);
		}
	}

	public List<AddressPersonGroupByCity> personsInCities() {
		List<AddressPersonGroupByCity> addresses = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(
						"SELECT city, COUNT(u.id_user) AS count FROM address a LEFT JOIN user u ON a.id_address=u.id_address GROUP BY city")) {
			while (rs.next()) {
				AddressPersonGroupByCity address = new AddressPersonGroupByCity();
				address.setCity(rs.getString("city"));
				address.setUserCount(rs.getLong("count"));
				addresses.add(address);
			}
			return addresses;
		} catch (SQLException e) {
			throw new DataAccessException("Addresses in cities not found", e);
		}
	}

}
