package com.cgi.data.dao;

import java.sql.*;
import java.util.*;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import com.cgi.api.PersonBasicViewDto;
import com.cgi.api.PersonDetailedViewDto;
import com.cgi.api.UserCreateRequestDto;
import com.cgi.data.exceptions.DataAccessException;

@Stateless
public class PersonDao {

	@Resource(lookup = "jdbc/bootcampCGI")
	private DataSource dataSource;

	public List<PersonBasicViewDto> findAll() {
		List<PersonBasicViewDto> persons = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT id_user, first_name, surname, nickname FROM user u")) {
			while (rs.next()) {
				PersonBasicViewDto person = new PersonBasicViewDto();
				person.setId(rs.getLong("id_user"));
				person.setFamilyName(rs.getString("surname"));
				person.setName(rs.getString("nickname"));
				person.setGivenName(rs.getString("first_name"));
				persons.add(person);
			}
			return persons;
		} catch (SQLException e) {
			throw new DataAccessException("Persons not found..", e);
		}
	}

	public PersonDetailedViewDto findById(Long id) {
		String findByIdSQL = "SELECT  id_user, first_name, surname, nickname FROM user u WHERE u.id_user = ?";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(findByIdSQL);) {
			ps.setLong(1, id);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					PersonDetailedViewDto person = new PersonDetailedViewDto();
					person.setId(rs.getLong("id_user"));
					person.setFamilyName(rs.getString("surname"));
					person.setName(rs.getString("nickname"));
					person.setGivenName(rs.getString("first_name"));
					return person;
				}
			}
		} catch (SQLException e) {
			throw new DataAccessException("Person with id: " + id + " not found.", e);
		}
		throw new DataAccessException("Person with id: " + id + " not found.");
	}

	public void createPerson(UserCreateRequestDto userCreate) {

		// ... insert...
	}

}
