package com.cgi.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.cgi.api.AddressBasicViewDto;
import com.cgi.api.AddressInfoViewDto;
import com.cgi.data.dao.AddressDao;

@Stateless
public class AddressService {

	@EJB
	private AddressDao addressDao;

	public List<AddressBasicViewDto> findAll() {
		return addressDao.findAll();
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public AddressInfoViewDto adressInfoView() {
		// external API
		AddressInfoViewDto addressInfoViewDto = new AddressInfoViewDto();
		addressInfoViewDto.setAddresses(addressDao.findAll());
		addressInfoViewDto.setPersonsInCities(addressDao.personsInCities());
		return addressInfoViewDto;
	}
}
