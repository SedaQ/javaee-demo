package com.cgi.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.cgi.api.PersonBasicViewDto;
import com.cgi.api.PersonDetailedViewDto;
import com.cgi.api.UserCreateRequestDto;
import com.cgi.data.dao.PersonDao;

@Stateless
public class PersonService {

	@EJB
	private PersonDao personDao;

	public List<PersonBasicViewDto> findAll() {
		return personDao.findAll();
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public PersonDetailedViewDto findById(Long id) {
		return personDao.findById(id);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void createPerson(UserCreateRequestDto userCreate) {
		personDao.createPerson(userCreate);
	}

//	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void print() {

	}
}
