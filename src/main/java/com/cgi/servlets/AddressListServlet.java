package com.cgi.servlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.AddressBasicViewDto;
import com.cgi.api.AddressInfoViewDto;
import com.cgi.service.AddressService;

/**
 * Servlet implementation class AddressListServlet
 */
@WebServlet(name = "AddressListServlet", urlPatterns = { "/address-list" })
public class AddressListServlet extends HttpServlet {

	@EJB
	private AddressService addressService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddressListServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		AddressInfoViewDto addresses = addressService.adressInfoView();
		request.setAttribute("addressInfo", addresses);
		request.getRequestDispatcher("/WEB-INF/jsp/address/AddressList.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
