package com.cgi.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.ApiError;
import com.cgi.data.exceptions.DataAccessException;
import com.cgi.exceptions.ResourceNotFoundException;

@WebServlet(name = "ErrorHandlerServlet", urlPatterns = { "/error-handler" })
public class ErrorHandlerServlet extends HttpServlet {

	// Method to handle GET method request.
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Analyze the servlet exception
		Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
		String servletName = (String) request.getAttribute("javax.servlet.error.servlet_name");

		ApiError apiError = new ApiError();
		if (throwable instanceof ResourceNotFoundException) {
			ResourceNotFoundException ex = (ResourceNotFoundException) throwable;
			apiError.setTimestamp(System.currentTimeMillis());
			apiError.setStatusCode(404);
			apiError.setMessage(ex.getMessage());
			apiError.setPath(request.getRequestURI());
			apiError.setError("...error...");
		} else {
			apiError.setTimestamp(System.currentTimeMillis());
			apiError.setStatusCode(500);
			apiError.setMessage("Internal server error");
			apiError.setPath(request.getRequestURI());
			apiError.setError("...error...");
		}
		request.setAttribute("error", apiError);

		request.getRequestDispatcher("/WEB-INF/jsp/error/Error.jsp").forward(request, response);
	}

	// Method to handle POST method request.
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
