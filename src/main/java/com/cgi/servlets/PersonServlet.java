package com.cgi.servlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.PersonBasicViewDto;
import com.cgi.api.PersonDetailedViewDto;
import com.cgi.exceptions.ResourceNotFoundException;
import com.cgi.service.PersonService;

/**
 * Servlet implementation class PersonServlet
 */
@WebServlet(name = "PersonServlet", urlPatterns = { "/person/*" })
public class PersonServlet extends HttpServlet {

	@EJB
	private PersonService personService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PersonServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		if (pathInfo != null && pathInfo.contains("/detail")) {
//			String id = request.getParameter("id");
//			PersonDetailedViewDto person = personService.findById(Long.parseLong(id));
			System.out.println("Id...: " + request.getParameter("id"));
			String idStr = request.getParameter("id");
			Long idLong = Long.parseLong(idStr);
			System.out.println(idLong);
			try {
				PersonDetailedViewDto person = personService.findById(idLong);
				request.setAttribute("person", person);
				request.getRequestDispatcher("/WEB-INF/jsp/person/Person.jsp").forward(request, response);
			} catch (Exception ex) {
				throw new ResourceNotFoundException(ex.getMessage());
			}
		} else if (pathInfo != null && pathInfo.contains("/list")) {
			List<PersonBasicViewDto> persons = personService.findAll();
			request.setAttribute("persons", persons);
			request.getRequestDispatcher("/WEB-INF/jsp/person/PersonList.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
