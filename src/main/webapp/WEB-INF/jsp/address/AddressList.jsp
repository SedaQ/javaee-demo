<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/jsp/common/Head.jsp"%>
<title>Hello world JSP</title>
</head>
<body>
	<%@ include file="/WEB-INF/jsp/common/Header.jsp"%>
	<div>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>id</th>
					<th>city</th>
					<th>street</th>
					<th>house number</th>
					<th>zip code</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${addressInfo.addresses}" var="address">
					<tr>
						<td><c:out value="${address.id}" /></td>
						<td><c:out value="${address.city}" /></td>
						<td><c:out value="${address.street}" /></td>
						<td><c:out value="${address.houseNumber}" /></td>
						<td><c:out value="${address.zipCode}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<p>Users in cities ...</p>
		<table>
			<thead>
				<tr>
					<th>city</th>
					<th>users count</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${addressInfo.personsInCities}"
					var="usersInCityCount">
					<tr>
						<td><c:out value="${usersInCityCount.city}" /></td>
						<td><c:out value="${usersInCityCount.userCount}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>