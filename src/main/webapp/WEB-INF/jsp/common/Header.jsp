<div class="header">
	<nav class="navbar navbar-inverse">
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="${pageContext.request.contextPath}/">Bootcamp
					CGI DEMO</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a
					href="${pageContext.request.contextPath}/">Bootcamp CGI DEMO</a></li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">Training <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="${pageContext.request.contextPath}/person/list">Persons</a></li>
						<li><a href="${pageContext.request.contextPath}/address-list">Addresses</a></li>
					</ul></li>
				<li><a href="${pageContext.request.contextPath}/about">About</a></li>
				<li><a href="${pageContext.request.contextPath}/contact">Contact</a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="${pageContext.request.contextPath}/logout"><span
						class="glyphicon glyphicon-log-in"> Logout</span></a></li>
			</ul>
		</div>
	</nav>
</div>