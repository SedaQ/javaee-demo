<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="Person">
	<jsp:attribute name="body">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>id</th>
				<th>name</th>
				<th>given name</th>
				<th>family name</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><c:out value="${person.id}" /></td>
				<td><c:out value="${person.name}" /></td>
				<td><c:out value="${person.givenName}" /></td>
				<td><c:out value="${person.familyName}" /></td>
			</tr>
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>